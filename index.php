<!DOCTYPE html>
<!--

                                          Mustafa Omran
                                    promustafaomran@hotmail.com

                       Single server web application used to check number 
                      --  using native css and javascript for font-end --
                               -- using PHP programming language --

-->
<html>
    
             <!-- header -->   
            <?php include './files/header.php'; ?>
 
        <body>
            <div id="header">Check your number phone</div>
        
        <!-- process --> 
        <?php include './files/process.php'; ?>
        
                

        <!-- footer -->
            <?php include './files/footer.php'; ?>
            <script src="js/script.js"></script>
            
       </body>
       
</html>
